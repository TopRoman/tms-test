from os import environ

from dotenv import load_dotenv
from flask_alembic import Alembic
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

load_dotenv('./app/.env')


class Config:
    APP_ENV = environ.get('APP_ENV')
    # Сгенерить ключ можно через secrets.token_urlsafe()
    SECRET_KEY = environ.get('SECRET_KEY')
    # Bcrypt по дефолту в SECURITY_PASSWORD_HASH нужна соль
    # Сгенерить соль можно через secrets.SystemRandom().getrandbits(128)
    SECURITY_PASSWORD_SALT = environ.get('SECURITY_PASSWORD_SALT')
    DATABASE = f"postgresql+psycopg2://{environ.get('POSTGRES_USER')}:{environ.get('POSTGRES_PASSWORD')}@{environ.get('POSTGRES_HOST')}:5432/{environ.get('POSTGRES_DB')}"
    SQLALCHEMY_DATABASE_URI = DATABASE
    SQLALCHEMY_DATABASE_URL = DATABASE
    SQLALCHEMY_ENGINE_OPTIONS = {"pool_pre_ping": True}
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True


class ProductionConfig(Config):
    DEBUG = environ.get('DEBUG')


class TestConfig(Config):
    DATABASE = f"postgresql+psycopg2://{environ.get('POSTGRES_USER')}:{environ.get('POSTGRES_PASSWORD')}@{environ.get('POSTGRES_HOST')}:5432/test"
    SQLALCHEMY_DATABASE_URI = DATABASE
    SQLALCHEMY_DATABASE_URL = DATABASE
    DEBUG = True
    TESTING = True

    @staticmethod
    def init_app(app):
        from app import alembic
        # migrate = Migrate()
        # migrate.init_app(app, db)

        app.app_context().push()

        alembic.init_app(app, run_mkdir=False)  # call in the app factory if you're using that pattern

        # print(alembic.config.alembic.__dict__)
        # print(app.extensions['sqlalchemy'].__dict__)
        # with app.app_context():
        #     alembic.upgrade()


config = {
    'dev': DevelopmentConfig,
    'prod': ProductionConfig,
    'test': TestConfig,
    'default': DevelopmentConfig
}
