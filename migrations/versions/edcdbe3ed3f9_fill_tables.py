"""fill_tables

Revision ID: edcdbe3ed3f9
Revises: ecfabee9762e
Create Date: 2023-12-19 12:32:40.952354

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa
from sqlalchemy import orm
from sqlalchemy.ext.declarative import declarative_base

# revision identifiers, used by Alembic.
revision: str = 'edcdbe3ed3f9'
down_revision: Union[str, None] = 'ecfabee9762e'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None

Base = declarative_base()


class TicketType(Base):
    __tablename__ = 'ticket_types'
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(64))


class LinkType(Base):
    __tablename__ = 'link_types'
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(64))


def upgrade() -> None:
    bind = op.get_bind()
    session = orm.Session(bind=bind)

    link_types = [
        LinkType(name='Child of'),
        LinkType(name='Parent of'),
    ]
    session.add_all(link_types)

    ticket_types = [
        TicketType(name='Test suit'),
    ]
    session.add_all(ticket_types)

    session.commit()


def downgrade() -> None:
    pass
