"""fill tables

Revision ID: 753487719e5d
Revises: 4c991bc802ff
Create Date: 2023-11-28 09:53:29.180685

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa
from sqlalchemy import orm
from sqlalchemy.ext.declarative import declarative_base

# revision identifiers, used by Alembic.
revision: str = '753487719e5d'
down_revision: Union[str, None] = '4c991bc802ff'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None

Base = declarative_base()


# согласно документации алембика - крайне не рекомендуется импортировать модели уже представленные в приложении
# для работы следует переобъявить их в миграции (можно даже не полностью все поля)
class Status(Base):
    __tablename__ = 'statuses'
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(64))


class UserGroup(Base):
    __tablename__ = 'user_groups'
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(64))
    active = sa.Column(sa.Boolean)


class TicketType(Base):
    __tablename__ = 'ticket_types'
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(64))


def upgrade() -> None:
    bind = op.get_bind()
    session = orm.Session(bind=bind)

    statuses = [
        Status(name='Отсутствует'),
        Status(name='В работе'),
        Status(name='Пауза'),
        Status(name='Успешно'),
        Status(name='Ошибка'),
        Status(name='Нет доступа'),
        Status(name='Перепроверен'),
        Status(name='Закрыт'),
    ]
    session.add_all(statuses)

    user_groups = [
        UserGroup(name='Администратор', active=True),
        UserGroup(name='Менеджер', active=True),
        UserGroup(name='Тестировщик', active=True),
    ]
    session.add_all(user_groups)

    ticket_types = [
        TicketType(name='Test case'),
        TicketType(name='Test run'),
    ]
    session.add_all(ticket_types)

    session.commit()


def downgrade() -> None:
    pass
