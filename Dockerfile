FROM python:3.11-slim

ENV PYTHONBUFFERED=1

WORKDIR /code

COPY ./requirements.txt /code/requirements.txt

RUN pip install -r /code/requirements.txt

COPY ./app /code/app
COPY ./migrations /code/migrations
COPY ./config.py /code/config.py
COPY ./flasky.py /code/flasky.py
COPY ./alembic.ini /code/alembic.ini
COPY ./pytest.ini /code/pytest.ini

EXPOSE 5000

CMD ["flask", "--app", "flasky:app", "run", "--host", "0.0.0.0", "--port", "5000", "--debug"]
