from app import create_app
from config import Config

# Ниже находится переменная приложения, что нужна для работы через консоль/докер.
# Более в этом файле ничего быть не должно - это просто "точка входа"

app = create_app(Config.APP_ENV)
