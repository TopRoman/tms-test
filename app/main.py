from flask import current_app as app, request, make_response, jsonify, Response
from flask_login import login_required, login_user, current_user, logout_user
from flask_swagger import swagger

from app import login_manager
from app.repository.user_repository import UserRepository


# импорт роутов из других модулей находится тут

@app.before_request
def handle_preflight():
    if request.method == "OPTIONS":
        res = Response()
        res.headers['X-Content-Type-Options'] = '*'
        return res


@app.route('/', methods=['GET'])
def index():
    if current_user.is_authenticated:
        return 'Hello! Its index page with ' + current_user.name + ' user'
    return 'Hello! Its index page with anonymous user'


@app.route("/spec")
def spec():
    swag = swagger(app)
    swag['info']['version'] = '1.0'
    swag['info']['title'] = 'API TMS'
    return jsonify(swag)


@login_manager.request_loader
def load_user_from_request(flask_request):
    api_token = flask_request.headers.get('Authorization')
    if api_token:
        user = UserRepository.find_by_token(api_token)
        if user:
            return user

    return None


@app.route('/login', methods=['POST'])
def login():
    name = request.form.get('name')
    password = request.form.get('password')
    user = UserRepository.get_user_by_name(name)
    if user and user.check_password(password) and user.is_active():
        login_user(user, remember=True)
        user_response = user.to_list()
        user_response['token'] = user.get_config().token
        return make_response(user_response, 200)
    return make_response('Login failed', 404)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return make_response('Logout success', 200)
