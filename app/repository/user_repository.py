import uuid

from app import db
from app.models import User
from app.schemas.user import UserCreate, UserUpdate, UserChangeProject


class UserRepository:
    @staticmethod
    def create_user(user_form: UserCreate):
        user = User()
        user.name = user_form.name
        user.set_password(user_form.password)
        user.active = user_form.active
        user.config = {}
        user_config = user.get_config()
        user_config.token = uuid.uuid4().__str__()
        user.user_group_id = None
        if user_form.email:
            user_config.email = user_form.email
        if user_form.fullname:
            user_config.fullname = user_form.fullname

        user.set_config(user_config)

        db.session.add(user)
        db.session.commit()
        db.session.refresh(user)

        return user

    @staticmethod
    def update_user(user_id: int, user_form: UserUpdate):
        user = UserRepository.get_user_by_id(user_id)
        if user:
            user.active = user_form.active
            user.name = user_form.name
            user_config = user.get_config()
            user_config.token = uuid.uuid4().__str__()
            user.set_config(user_config)
            if user_form.user_group_id:
                user.user_group_id = user_form.user_group_id
            if user_form.password:
                user.set_password(user_form.password)
            if user_form.email:
                user_config.email = user_form.email
            if user_form.fullname:
                user_config.fullname = user_form.fullname
            db.session.add(user)
            db.session.commit()
            db.session.refresh(user)

            return user
        return False

    @staticmethod
    def change_project(user_id: int, user_form: UserChangeProject):
        user = UserRepository.get_user_by_id(user_id)
        if user:
            user_config = user.get_config()
            user_config.project_id = int(user_form.project_id)
            user.set_config(user_config)
            db.session.add(user)
            db.session.commit()
            db.session.refresh(user)

            return user
        return False

    @staticmethod
    def delete_user(user_id: int):
        user = UserRepository.get_user_by_id(user_id)

        if user:
            db.session.delete(user)
            db.session.commit()
            return True
        else:
            return False

    @staticmethod
    def get_user_by_id(user_id: int):
        return db.session.query(User).get(user_id)

    @staticmethod
    def get_user_by_name(user_name: str):
        return db.session.query(User).filter(User.name == user_name).first()

    @staticmethod
    def get_user_query():
        return db.session.query(User)

    @staticmethod
    def find_by_token(api_token):
        return db.session.query(User).filter(User.config['token'].as_string() == api_token).first()
