from app.api import api


@api.route('/', methods=['GET'])
def index():
    """
    Тестовый эндпоинт api
    ---
    tags:
        - test
    responses:
        200:
            description: Эндпоинт отработает штатно
    """
    return 'Hello! Its API index page!'
