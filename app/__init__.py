from flask import Flask
from flask_alembic import Alembic
from flask_cors import CORS
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from jinja2 import Environment, FileSystemLoader

from config import config

# Если нужна пре-инициализация (или нужный до приложения инит) - лежит тут, перед фабрикой create_app.
db = SQLAlchemy()
alembic = Alembic()
login_manager = LoginManager()
env = Environment(loader=FileSystemLoader('templates'))


def create_app(config_name):
    _app = Flask(__name__)
    _app.config.from_object(config[config_name])
    db.init_app(_app)
    config[config_name].init_app(_app)
    login_manager.init_app(_app)
    CORS(_app)

    # Регистрация блюпринтов, префиксы для урлов указываются тут
    from app.api import api as api_blueprint
    _app.register_blueprint(api_blueprint, url_prefix='/api')
    from app.commands import console
    _app.register_blueprint(console)

    # Импорт роутов. На деле кроме импорта main-а ничего быть не должно, если нужно добавить роут - докиньте в main.
    # Если нужно инициализировать роуты библиотек, тогда их следует регистрировать тут.
    # Роут объявленный повторно - будет игнорироваться.
    with _app.app_context():
        import app.main

        # подключение swaggerUI
        from swagger_ui import api_doc
        api_doc(_app, config_spec=app.main.spec().get_data(), url_prefix='/api/doc', title='Swagger')
    return _app

# Если нужны еще какие-нибудь иниты/настройки - ниже добавьте
