import json

from app.exceptions.domain import UnknownLinkType
from app.models import Ticket, LinkType, Link
from app.repository.link_repository import LinkRepository
from app.repository.link_type_repository import LinkTypeRepository
from app.schemas.link import LinkCreate


class LinkService:
    TYPE_PARENT_OF = LinkType.TYPE_PARENT_OF
    TYPE_CHILD_OF = LinkType.TYPE_CHILD_OF
    TYPE_BASED_ON = LinkType.TYPE_BASED_ON

    @staticmethod
    def link_tickets(first_ticket: Ticket, link_type: str, second_ticket: Ticket):
        if link_type == LinkType.TYPE_PARENT_OF:
            LinkService.__create_link(first_ticket, LinkType.TYPE_PARENT_OF, second_ticket)
            LinkService.__create_link(second_ticket, LinkType.TYPE_CHILD_OF, first_ticket)
            return

        if link_type == LinkType.TYPE_CHILD_OF:
            LinkService.__create_link(first_ticket, LinkType.TYPE_CHILD_OF, second_ticket)
            LinkService.__create_link(second_ticket, LinkType.TYPE_PARENT_OF, first_ticket)
            return

        if link_type == LinkType.TYPE_BASED_ON:
            LinkService.__create_link(first_ticket, LinkType.TYPE_BASED_ON, second_ticket)
            return

        raise UnknownLinkType()

    @staticmethod
    def drop_link(link: Link):
        link_type = link.link_type.name

        if link_type == LinkType.TYPE_PARENT_OF:
            child_link = LinkService.__find_paired_link(link)
            if child_link:
                LinkRepository.delete_link(child_link.id)
            LinkRepository.delete_link(link.id)
            return

        if link_type == LinkType.TYPE_CHILD_OF:
            parent_link = LinkService.__find_paired_link(link)
            if parent_link:
                LinkRepository.delete_link(parent_link.id)
            LinkRepository.delete_link(link.id)
            return

        if link_type == LinkType.TYPE_BASED_ON:
            LinkRepository.delete_link(link.id)
            return

        raise UnknownLinkType()

    @staticmethod
    def __find_paired_link(link: Link):
        origin_ticket = link.get_origin_ticket()
        linked_ticket = link.get_linked_ticket()

        paired_link_url = json.dumps({'key_id': origin_ticket.key_id, 'project_id': origin_ticket.project_id})
        paired_link = (LinkRepository.get_link_query()
                       .filter_by(key_id=linked_ticket.key_id, project_id=linked_ticket.project_id, url=paired_link_url)
                       .first())

        return paired_link

    # first_ticket - тот тикет к которому будем привязывать (например родитель для связи Parent_of).
    # link_type - строковое обозначение связи (из констант в LinkType).
    # second_ticket - тот тикет, который будем привязывать (например ребенок для связи Parent_of).
    @staticmethod
    def __create_link(first_ticket: Ticket, link_type: str, second_ticket: Ticket):
        link_type_model = LinkTypeRepository.find_link_type_by_name(link_type)
        if not link_type_model:
            link_type_model = LinkType()
            link_type_model.name = link_type

        link_scheme = LinkCreate(**{
            'key_id': first_ticket.key_id,
            'project_id': first_ticket.project_id,
            'link_type_id': link_type_model.id,
            'url': json.dumps({'key_id': second_ticket.key_id, 'project_id': second_ticket.project_id}),
        })

        LinkRepository.create_link(link_scheme)
