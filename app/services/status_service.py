from app.models import Status


class StatusService:
    @staticmethod
    def allowed_statuses(selected_status: str):
        if selected_status == Status.STATUS_MISSING:
            return [
                selected_status,
                Status.STATUS_IN_PROGRESS
            ]

        if selected_status == Status.STATUS_IN_PROGRESS:
            return [
                selected_status,
                Status.STATUS_PAUSE,
                Status.STATUS_MISSING,
                Status.STATUS_NO_ACCESS,
                Status.STATUS_SUCCESS,
                Status.STATUS_ERROR,
            ]

        if selected_status == Status.STATUS_PAUSE:
            return [
                selected_status,
                Status.STATUS_NO_ACCESS,
                Status.STATUS_SUCCESS,
                Status.STATUS_ERROR,
            ]

        if selected_status == Status.STATUS_ERROR:
            return [
                selected_status,
                Status.STATUS_RECHECKED,
            ]

        if selected_status == Status.STATUS_NO_ACCESS:
            return [
                selected_status,
                Status.STATUS_PAUSE,
                Status.STATUS_SUCCESS,
                Status.STATUS_ERROR,
            ]

        return [
            selected_status
        ]

    @staticmethod
    def can_change(old_status: str, new_status: str):
        allowed_statuses = StatusService.allowed_statuses(old_status)
        if new_status in allowed_statuses:
            return True
        return False
