from app.models import Status


class Statistic:
    @staticmethod
    def get_test_result_stat(test_results):
        stat = {Status.STATUS_MISSING: 0,
                Status.STATUS_IN_PROGRESS: 0,
                Status.STATUS_PAUSE: 0,
                Status.STATUS_SUCCESS: 0,
                Status.STATUS_ERROR: 0,
                Status.STATUS_NO_ACCESS: 0,
                Status.STATUS_RECHECKED: 0,
                Status.STATUS_CLOSED: 0,
                Status.STATUS_OPENED: 0, }
        for test_result in test_results:
            stat[test_result.status.name] = stat[test_result.status.name] + 1

        try:
            result = (
                    (
                            (
                                    stat[Status.STATUS_SUCCESS] +
                                    stat[Status.STATUS_ERROR] +
                                    stat[Status.STATUS_NO_ACCESS] +
                                    stat[Status.STATUS_RECHECKED]
                            ) / (
                                    stat[Status.STATUS_SUCCESS] +
                                    stat[Status.STATUS_ERROR] +
                                    stat[Status.STATUS_NO_ACCESS] +
                                    stat[Status.STATUS_RECHECKED] +
                                    stat[Status.STATUS_MISSING] +
                                    stat[Status.STATUS_IN_PROGRESS] +
                                    stat[Status.STATUS_PAUSE] +
                                    stat[Status.STATUS_CLOSED] +
                                    stat[Status.STATUS_OPENED]
                            )
                    ) * 100
            )
            stat['Пройдено'] = round(result)
        except ZeroDivisionError:
            stat['Пройдено'] = 0

        return stat
