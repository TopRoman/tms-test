from app import db


class Pagination:
    PER_PAGE = 20

    def paginate_db(self, query, page_number: int, per_page: int | None = None):
        if not per_page:
            per_page = self.PER_PAGE

        items_raw = db.paginate(query, page=page_number, per_page=per_page)
        items = [item.to_list() for item in items_raw.items]

        return self.__format_result(items_raw.total, page_number, per_page, items)

    def paginate_list(self, items: list, page_number: int = 1, per_page: int | None = None):
        items_count = len(items)
        if not per_page:
            per_page = self.PER_PAGE

        start = ((page_number - 1) * per_page)
        end = (page_number * per_page)
        items_slice = items[start:end]

        return self.__format_result(items_count, page_number, per_page, items_slice)

    def __format_result(self, items_count: int, page_number: int, per_page: int, items: list):
        return {
            'page': page_number,
            'total': items_count,
            'per_page': per_page,
            'items': items
        }
