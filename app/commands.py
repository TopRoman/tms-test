import click
from flask import Blueprint
from app.repository.user_repository import UserRepository
from app.schemas.user import UserCreate

console = Blueprint('console', 'app.cli')


@console.cli.command("create-user")
@click.argument("name")
@click.argument("password")
def create_user(name, password):
    user_schema = UserCreate(name=name, password=password, active=True, email='test@mart.ru', fullname='test',
                             user_group_id=None)
    UserRepository.create_user(user_schema)
