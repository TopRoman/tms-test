"""fill_tables

Revision ID: a0543e65a567
Revises: edcdbe3ed3f9
Create Date: 2023-12-21 08:36:49.059134

"""
from typing import Sequence, Union

import sqlalchemy as sa
from alembic import op
from sqlalchemy import orm
from sqlalchemy.ext.declarative import declarative_base

# revision identifiers, used by Alembic.
revision: str = 'a0543e65a567'
down_revision: Union[str, None] = 'edcdbe3ed3f9'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None

Base = declarative_base()


class LinkType(Base):
    __tablename__ = 'link_types'
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(64))


def upgrade() -> None:
    bind = op.get_bind()
    session = orm.Session(bind=bind)

    link_types = [
        LinkType(name='Based on'),
    ]
    session.add_all(link_types)

    session.commit()


def downgrade() -> None:
    pass
