"""fill_tables

Revision ID: 9c79a1aa21e7
Revises: f0212c9c1c96
Create Date: 2024-01-10 09:45:43.950061

"""
from typing import Sequence, Union

import sqlalchemy as sa
from alembic import op
from sqlalchemy import orm
from sqlalchemy.ext.declarative import declarative_base

# revision identifiers, used by Alembic.
revision: str = '9c79a1aa21e7'
down_revision: Union[str, None] = 'f0212c9c1c96'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None

Base = declarative_base()


# согласно документации алембика - крайне не рекомендуется импортировать модели уже представленные в приложении
# для работы следует переобъявить их в миграции (можно даже не полностью все поля)
class Status(Base):
    __tablename__ = 'statuses'
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(64))


def upgrade() -> None:
    bind = op.get_bind()
    session = orm.Session(bind=bind)

    statuses = [
        Status(name='Открыт'),
    ]
    session.add_all(statuses)

    session.commit()


def downgrade() -> None:
    pass
