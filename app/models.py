import enum
import json
import uuid

from flask_login import UserMixin
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import relationship, backref
from werkzeug.security import generate_password_hash, check_password_hash

from app import db


class Project(db.Model):
    __tablename__ = 'projects'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    key = db.Column(db.String(64), unique=True)
    desc = db.Column(db.Text)
    config = db.Column(JSONB)
    next_key_id = db.Column(db.Integer)

    class ProjectConfig:
        def __init__(self, **data) -> None:
            self.__dict__.update(data)

    def set_config(self, config: ProjectConfig) -> None:
        self.config = config.__dict__

    def get_config(self) -> ProjectConfig:
        return Project.ProjectConfig(**self.config)

    def to_list(self):
        return {
            'id': self.id,
            'name': self.name,
            'key': self.key,
            'desc': self.desc,
            'config': self.config,
            'next_key_id': self.next_key_id,
        }


tickets_tags = db.Table(
    'tickets_tags',
    db.Column('tag_id', db.Integer(), db.ForeignKey('tags.id')),
    db.Column('ticket_key_id', db.Integer()),
    db.Column('ticket_project_id', db.Integer()),
    db.ForeignKeyConstraint(
        ["ticket_key_id", "ticket_project_id"], ["tickets.key_id", "tickets.project_id"]
    )
)


class Ticket(db.Model):
    __tablename__ = 'tickets'
    key_id = db.Column(db.Integer, primary_key=True)
    version = db.Column(db.Integer)
    project_id = db.Column(db.Integer, db.ForeignKey('projects.id'), primary_key=True)
    name = db.Column(db.String(64))
    desc = db.Column(db.Text)
    comment = db.Column(db.Text)
    ticket_type_id = db.Column(db.Integer, db.ForeignKey('ticket_types.id'))
    status_id = db.Column(db.Integer, db.ForeignKey('statuses.id'))
    resolution_id = db.Column(db.Integer, db.ForeignKey('resolutions.id'))
    created_at = db.Column(db.DateTime(), default=db.func.now(), nullable=False)
    created_by = db.Column(db.Integer, db.ForeignKey('users.id'))
    updated_at = db.Column(db.DateTime(), default=db.func.now(), onupdate=db.func.current_timestamp(), nullable=False)
    updated_by = db.Column(db.Integer, db.ForeignKey('users.id'))
    assign_to = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=True)
    attrib = db.Column(JSONB)
    tags = relationship('Tag', secondary=tickets_tags,
                        backref=backref('tags', lazy='dynamic'))
    links = db.relationship('Link', backref='ticket')
    test_case_test_results = db.relationship('TestResult', backref='test_case',
                                             foreign_keys='TestResult.test_case_id, TestResult.project_id')
    test_run_test_results = db.relationship('TestResult', backref='test_run',
                                            foreign_keys='TestResult.test_run_id, TestResult.project_id')

    def __init__(self, **data) -> None:
        self.__dict__.update(data)

    class TicketAttrib:
        def __init__(self, **data) -> None:
            self.is_root = None
            self.priority = None
            self.type = None
            self.__dict__.update(data)

        is_root: bool | None = None
        priority: str | None = None
        type: str | None = None

    def set_attrib(self, attrib: TicketAttrib) -> None:
        self.attrib = attrib.__dict__

    def get_attrib(self) -> TicketAttrib:
        return Ticket.TicketAttrib(**self.attrib)

    def set_tags(self, ids: list) -> None:
        self.tags.clear()
        for _id in ids:
            tag = db.session.query(Tag).get(_id)
            self.tags.append(tag)

    def get_tag_ids(self) -> list[int]:
        tags = self.tags
        result = list()
        for tag in tags:
            result.append(tag.id)

        return result

    def get_parent_suit(self):
        for parent in self.get_parents():
            if parent.get_type() == TicketType.TYPE_TEST_SUIT:
                return parent

        return None

    @staticmethod
    def get_parent_suit_ids(ticket, test_suit_list=None):
        if not test_suit_list:
            test_suit_list = []
        for parent in ticket.get_parents():
            if parent.get_type() == TicketType.TYPE_TEST_SUIT:
                test_suit_list.append(parent.key_id)
                ticket.get_parent_suit_ids(parent, test_suit_list)
        return test_suit_list

    def get_type(self):
        return self.ticket_type.name

    def get_children(self):
        result = []

        for link in self.links:
            if link.link_type.name == LinkType.TYPE_PARENT_OF:
                result.append(link.get_linked_ticket())

        return result

    def get_parents(self):
        result = []

        for link in self.links:
            if link.link_type.name == LinkType.TYPE_CHILD_OF:
                result.append(link.get_linked_ticket())

        return result

    def get_based_on_tickets(self):
        result = []

        for link in self.links:
            if link.link_type.name == LinkType.TYPE_BASED_ON:
                result.append(link.get_linked_ticket())

        return result

    def get_suit(self):
        for parent in self.get_parents():
            if parent.get_type() == TicketType.TYPE_TEST_SUIT:
                return parent.to_short_list()

    def to_list(self):
        ticket_list = {
            # todo возможно стоит отдавать не айдишники, а сущности
            'key_id': self.key_id,
            'version': self.version,
            'project_id': self.project_id,
            'name': self.name,
            'desc': self.desc,
            'comment': self.comment,
            'ticket_type_id': self.ticket_type_id,
            'status_id': self.status_id,
            'status': self.status.to_list(),
            'resolution_id': self.resolution_id,
            'created_at': self.created_at,
            'created_by': self.created_by,
            'author': self.author.to_short_list(),
            'updated_at': self.updated_at,
            'updated_by': self.updated_by,
            'assign': self.assign.to_short_list() if self.assign_to else None,
            'attrib': self.attrib,
            'tags': self.get_tag_ids(),
            'suit': self.get_suit(),
        }

        if self.get_type() == TicketType.TYPE_TEST_RUN:
            from app.services.statistic import Statistic
            if self.test_run_test_results:
                ticket_list['statistic'] = Statistic.get_test_result_stat(self.test_run_test_results)
            else:
                ticket_list['statistic'] = None

        return ticket_list

    def to_short_list(self):
        return {
            'name': self.name,
            'type': self.get_type(),
            'status': self.status.to_list(),
            'key_id': self.key_id,
            'project_id': self.project_id,
        }


class TicketHistory(db.Model):
    __tablename__ = 'ticket_histories'
    key_id = db.Column(db.Integer, primary_key=True)
    version = db.Column(db.Integer, primary_key=True)
    project_id = db.Column(db.Integer, db.ForeignKey('projects.id'), primary_key=True)
    based_on = db.Column(db.Integer)
    updated_at = db.Column(db.DateTime(), default=db.func.now(), onupdate=db.func.current_timestamp(), nullable=False)
    updated_by = db.Column(db.Integer, db.ForeignKey('users.id'))
    ticket_data = db.Column(JSONB)

    def get_ticket(self):
        dict_ticket = json.loads(self.ticket_data)
        return Ticket(**dict_ticket)

    def to_list(self):
        return {
            'key_id': self.key_id,
            'version': self.version,
            'project_id': self.project_id,
            'based_on': self.based_on,
            'updated_at': self.updated_at,
            'updated_by': self.updated_by,
            'ticket_data': json.loads(self.ticket_data),
        }


class TestResult(db.Model):
    __tablename__ = 'test_results'
    id = db.Column(db.Integer, primary_key=True)
    project_id = db.Column(db.Integer, db.ForeignKey('projects.id'))
    test_run_id = db.Column(db.Integer)
    test_case_id = db.Column(db.Integer)
    created_at = db.Column(db.DateTime(), default=db.func.now(), nullable=False)
    created_by = db.Column(db.Integer, db.ForeignKey('users.id'))
    assign_to = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=True)
    status_id = db.Column(db.Integer, db.ForeignKey('statuses.id'))
    comment = db.Column(db.Text)
    attrib = db.Column(JSONB)
    __table_args__ = (db.ForeignKeyConstraint([test_run_id, project_id],
                                              [Ticket.key_id, Ticket.project_id]),
                      db.ForeignKeyConstraint([test_case_id, project_id],
                                              [Ticket.key_id, Ticket.project_id]),
                      {})

    class TestResultAttrib:
        def __init__(self, **data) -> None:
            self.version = None
            self.__dict__.update(data)

        version = int | None

    def set_attrib(self, attrib: TestResultAttrib) -> None:
        self.attrib = attrib.__dict__

    def get_attrib(self) -> TestResultAttrib:
        return TestResult.TestResultAttrib(**self.attrib)

    def to_list(self):
        version = self.get_attrib().version
        if version:
            # если была заблокирована версия - то поля тест-кейса возвращаем от его версии
            history_record = TicketHistory.query.filter(TicketHistory.key_id == self.test_case.key_id,
                                                        TicketHistory.project_id == self.test_case.project_id,
                                                        TicketHistory.version == version).first()
            test_case = json.loads(history_record.ticket_data)
        else:
            # иначе возвращаем последнюю (актуальную)
            test_case = self.test_case.to_list()

        return {
            'id': self.id,
            'project_id': self.project_id,
            'test_run_id': self.test_run_id,
            'test_run': self.test_run.to_short_list(),
            'test_case_id': self.test_case_id,
            'test_case': test_case,
            'status_id': self.status_id,
            'status': self.status.to_list(),
            'comment': self.comment,
            'created_at': self.created_at,
            'created_by': self.created_by,
            'assign': self.assign.to_short_list() if self.assign_to else None,
            'author': self.author.to_short_list(),
            'attrib': self.attrib
        }


class Link(db.Model):
    __tablename__ = 'links'
    id = db.Column(db.Integer, primary_key=True)
    key_id = db.Column(db.Integer)
    project_id = db.Column(db.Integer)
    link_type_id = db.Column(db.Integer, db.ForeignKey('link_types.id'))
    url = db.Column(db.String(64))
    __table_args__ = (db.ForeignKeyConstraint([key_id, project_id],
                                              [Ticket.key_id, Ticket.project_id]),
                      {})

    # основной тикет, к которому привязывали другой тикет (например родитель для связи Parent_of)
    def get_origin_ticket(self):
        return Ticket.query.filter_by(key_id=self.key_id, project_id=self.project_id).first()

    # вспомогательный тикет, который привязывали ссылкой к основному (например ребенок для связи Parent_of)
    def get_linked_ticket(self):
        url_dict = json.loads(self.url)
        key_id = url_dict['key_id']
        project_id = url_dict['project_id']
        return Ticket.query.filter_by(key_id=key_id, project_id=project_id).first()


class Tag(db.Model):
    __tablename__ = 'tags'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    config = db.Column(JSONB)

    class TagConfig:
        def __init__(self, **data) -> None:
            self.__dict__.update(data)

    def set_config(self, config: TagConfig) -> None:
        self.config = config.__dict__

    def get_config(self) -> TagConfig:
        return Tag.TagConfig(**self.config)

    def to_list(self):
        return {
            'id': self.id,
            'name': self.name,
            'config': self.config,
        }


class TicketType(db.Model):
    __tablename__ = 'ticket_types'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    config = db.Column(JSONB)
    tickets = db.relationship('Ticket', backref='ticket_type')

    TYPE_TEST_SUIT = 'Test suit'
    TYPE_TEST_CASE = 'Test case'
    TYPE_TEST_RUN = 'Test run'


class ResourceTypeEnum(enum.Enum):
    # в энамках значения никак не используются, в базе будут только ключи
    test = 1


class LinkType(db.Model):
    __tablename__ = 'link_types'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    resource_type = db.Column(db.Enum(ResourceTypeEnum))
    links = db.relationship('Link', backref='link_type')

    TYPE_CHILD_OF = 'Child of'
    TYPE_PARENT_OF = 'Parent of'
    TYPE_BASED_ON = 'Based on'


class UserGroup(db.Model):
    __tablename__ = 'user_groups'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    active = db.Column(db.Boolean)
    config = db.Column(JSONB)
    users = db.relationship('User', backref='group', foreign_keys='User.user_group_id')

    # todo группа администратора есть, но не используется в данный момент
    GROUP_ADMIN = 'Администратор'
    GROUP_MANAGER = 'Менеджер'
    GROUP_TESTER = 'Тестировщик'

    class GroupConfig:
        def __init__(self, **data) -> None:
            self.__dict__.update(data)

    def set_config(self, config: GroupConfig) -> None:
        self.config = config.__dict__

    def get_config(self) -> GroupConfig:
        return UserGroup.GroupConfig(**self.config)

    def to_list(self):
        return {
            'id': self.id,
            'name': self.name,
            'active': self.active,
            'config': self.config
        }

    def to_short_list(self):
        return {
            'id': self.id,
            'name': self.name
        }


class User(db.Model, UserMixin):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    password = db.Column(db.String(255))
    user_group_id = db.Column(db.Integer, db.ForeignKey('user_groups.id'))
    active = db.Column(db.Boolean)
    config = db.Column(JSONB)
    ticket_author = db.relationship('Ticket', backref='author', foreign_keys='Ticket.created_by')
    ticket_assign = db.relationship('Ticket', backref='assign', foreign_keys='Ticket.assign_to')
    test_result_author = db.relationship('TestResult', backref='author', foreign_keys='TestResult.created_by')
    test_result_assign = db.relationship('TestResult', backref='assign', foreign_keys='TestResult.assign_to')

    class UserConfig:
        def __init__(self, **data) -> None:
            self.email = None
            self.fullname = None
            self.token = None
            self.project_id = None
            self.__dict__.update(data)

        email: str | None = None
        fullname: str | None = None
        token: str | None = None
        project_id: int | None = None

    def set_config(self, config: UserConfig) -> None:
        self.config = config.__dict__

    def get_config(self) -> UserConfig:
        return User.UserConfig(**self.config)

    def get_token(self):
        config = self.get_config()
        token = config.token
        if not token:
            # исключительный случай, когда у юзера почему-то не оказалось токена
            token = uuid.uuid4().__str__()
            config.token = token
            self.set_config(config)
            db.session.commit()
        return token

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def is_active(self):
        return self.active

    def to_list(self):
        return {
            'id': self.id,
            'name': self.name,
            'active': self.active,
            # выводим через геттер, чтобы незаполненные поля были со значением null
            'config': self.get_config().__dict__,
            'token': self.get_token(),
            'group': self.group.to_short_list()
        }

    def to_short_list(self):
        return {
            'id': self.id,
            'name': self.name,
            'active': self.active,
            'fullname': self.get_config().fullname,
            'email': self.get_config().email,
            'group': self.group.to_short_list()
        }


class Status(db.Model):
    __tablename__ = 'statuses'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    tickets = db.relationship('Ticket', backref='status')
    test_results = db.relationship('TestResult', backref='status')

    STATUS_MISSING = 'Отсутствует'
    STATUS_IN_PROGRESS = 'В работе'
    STATUS_PAUSE = 'Пауза'
    STATUS_SUCCESS = 'Успешно'
    STATUS_ERROR = 'Ошибка'
    STATUS_NO_ACCESS = 'Нет доступа'
    STATUS_RECHECKED = 'Перепроверен'
    STATUS_CLOSED = 'Закрыт'
    STATUS_OPENED = 'Открыт'

    def to_list(self):
        return {
            'id': self.id,
            'name': self.name
        }

    @staticmethod
    def get_opened_list():
        return [
            Status.STATUS_MISSING,
            Status.STATUS_IN_PROGRESS,
            Status.STATUS_PAUSE,
            Status.STATUS_NO_ACCESS,
            Status.STATUS_OPENED,
        ]

    @staticmethod
    def get_closed_list():
        return [
            Status.STATUS_SUCCESS,
            Status.STATUS_ERROR,
            Status.STATUS_RECHECKED,
        ]


class Resolution(db.Model):
    __tablename__ = 'resolutions'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))

    def to_list(self):
        return {
            'id': self.id,
            'name': self.name
        }


class AlembicVersion(db.Model):
    __tablename__ = 'alembic_version'
    version_num = db.Column(db.String(32), primary_key=True)
