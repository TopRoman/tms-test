from pydantic import BaseModel, Json


class UserConfig(BaseModel):
    email: str | None
    fullname: str | None


class UserCreate(BaseModel):
    name: str
    password: str
    user_group_id: int | None = None
    active: bool
    email: str | None
    fullname: str | None


class UserUpdate(UserCreate):
    password: str | None


class UserChangeProject(BaseModel):
    project_id: str | None
