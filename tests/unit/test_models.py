from app import db
from app.models import User
from app.repository.user_repository import UserRepository
from app.schemas.user import UserCreate


def test_new_user(app):
    print('test_new_user')
    """
    GIVEN a User model
    WHEN a new User is created
    THEN check the email, hashed_password, and role fields are defined correctly
    """
    user_schema = UserCreate(name='name', password='password', active=True, email='test@mart.ru', fullname='testname',
                             user_group_id=None)

    # with app.app_context():
    print('test_new_user - UserRepository.create_user BEFORE')

    user = UserRepository.create_user(user_schema)
    print('test_new_user - UserRepository.create_user')
    assert user.get_config().email == 'test@mart.ru'
    assert user.name == 'name'
    # assert user.password == 'password'
    assert user.active == True
    assert user.get_config().fullname == 'testname'

#
# def inc(x):
#     return x + 1
#
#
# def test_answer():
#     assert inc(4) == 5
