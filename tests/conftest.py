from pathlib import Path

import pytest
import sqlalchemy
from _pytest.fixtures import FixtureRequest
# import alembic.config
from alembic import command
from flask_migrate import Migrate

from app import create_app, db, alembic


@pytest.fixture()
def app():
    print('app fixture')
    app = create_app('test')
    # app.app_context().push()

    # other setup can go here

    # with app.app_context():
    alembic.upgrade()
    # db.session.commit()

    print('app fixture alembic.upgrade()')
    yield app

    # clean up / reset resources here

    # op.drop_table("alembic_version")
    db.drop_all()
    print('app fixture db.drop_all()')


@pytest.fixture()
def client(app):
    return app.test_client()


@pytest.fixture()
def runner(app):
    return app.test_cli_runner()


# @pytest.fixture()
# def database_engine():
#     return db.engine
