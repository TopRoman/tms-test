# TMS
Проект для сопровождения разработки.

## Разворачивание
* Создать .env из .env.example файлы в корне проекта и в корне приложения
* Запустить docker-compose
* Запустить из терминала команду `pip install -r requirements.txt` (в докере при запуске композа запустится сама)
* Запустить миграции `alembic upgrade head`

### Фласк
Для обращения к фласку требуется указывать путь до приложения. Делается это через параметр `--app='flasky:app'`.
Например, для просмотра зарегистрированных роутов можно использовать команду `flask --app='flasky:app' routes`

Ссылка на доку пакета сваггера: https://github.com/getsling/flask-swagger
Спецификация сваггера доступна по пути /spec
Ссылка на доку пакета сваггерUI: https://github.com/PWZER/swagger-ui-py
Cваггер UI доступен по пути /api/doc

### Вход в адмайнер
* система - PostgreSQL
* сервер - название контейнера бд (postgres)
* юзер - как во внешнем энве (DOCKER_POSTGRES_USER, если не менялся с дефолтного энва - user)
* пароль - как во внешнем энве (DOCKER_POSTGRES_PASSWORD, если не менялся с дефолтного энва - password)
* база - как во внешнем энве (DOCKER_POSTGRES_DB, если не менялся с дефолтного энва - database)

### Команды алембика
* `alembic upgrade head` - применение миграций
* `alembic downgrade -1` - откат на 1
* `alembic revision --autogenerate` - автогенерация по моделям

### Команды
* `flask --app='flasky:app' console create-user <login> <password>` - команда создания пользователя